
#ifndef FILE_IO
#define FILE_IO

#include <stdio.h>

FILE* open_file(const char* fname, const char* mode);

void close_file( FILE* file);

#endif
