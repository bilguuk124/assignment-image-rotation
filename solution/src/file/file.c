
#include <stdio.h>
#include <stdlib.h>

FILE* open_file(const char* fname, const char* mode){
    return fopen(fname,mode);
}

void close_file (FILE* file){
    if(file != NULL){
        fclose(file);
    }
}
