
#define DWORD 4
#define COLOR_DEPTH 24
#define BMP_HEADER_SIZE 40
#define BMP_RESERVED 0
#define BMP_TYPE 0x4D42
#define BMP_PLANES 1


#include "bmp-util/bmp-util.h"
#include "bmp-util/bmp_io.h"
#include "image/image.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static enum read_status read_bmp_header(FILE* in, struct bmp_header* header){
    if (fseek(in, 0, SEEK_END) != 0){
        return READ_FAILED;
    }
    size_t size = ftell(in);
    if(size < sizeof(struct bmp_header)){
        return READ_INVALID_HEADER;
    }
    rewind(in);
    if(fread(header, sizeof(struct bmp_header), 1, in) != 1){
        return READ_FAILED;
    }
    return READ_OK;
}

enum read_status read_pixels(struct image* img, FILE* in, uint8_t padding){
    size_t width = img->width;
    size_t height = img->height;
    struct pixel* pixels = malloc (width * height * sizeof(struct pixel));
    for( size_t i = 0; i < height; i ++){
        fread(pixels+i*width, sizeof(struct pixel), width, in);
        fseek(in, padding, SEEK_CUR);
    }
    img->pixels = pixels;
    return READ_OK;
}



static uint32_t get_padding(uint32_t width){
    if(width %4 == 0){
        return 0;
    }
    else{
        return DWORD - (width*(COLOR_DEPTH / 8)) % DWORD;
    }
}

enum read_status from_bmp(FILE* in, struct image* img){
    struct bmp_header header = {0};
    enum read_status status = read_bmp_header(in, &header);
    if (status != READ_OK) return status;
    const uint32_t width = header.biWidth;
    const uint32_t height = header.biHeight;
    const uint32_t padding = get_padding(width);
    img->width = width;
    img->height = height;
    return read_pixels(img, in, padding);
}

enum write_status write_header(FILE* const out, const size_t width, const size_t height){
    uint8_t padding = get_padding(width);
    const size_t image_size = (sizeof(struct pixel) * (width) + padding)* height;
    const struct bmp_header header = {
        .bfType = 0x4D42,
        .bfileSize = sizeof(struct bmp_header) + image_size,
        .bfReserved = BMP_RESERVED,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BMP_HEADER_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = BMP_PLANES,
        .biBitCount = COLOR_DEPTH,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biCompression = 0,
        .biSizeImage = image_size,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };
    
    if ( fwrite(&header, sizeof(struct bmp_header), 1, out) != 1){
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image* img){
    if (!out || !img) return WRITE_ERROR;
    const uint32_t width = img->width;
    const uint32_t height = img->height;
    const enum write_status status = write_header(out, width, height);
    if (status != WRITE_OK) return status;
    uint8_t padding = get_padding(width);
    uint8_t* const paddings[3] = {0};
    for (size_t i = 0; i < height; i++){
        if (!fwrite((img->pixels) + i * width, sizeof(struct pixel)* width, 1, out)) return WRITE_ERROR;
        if (!fwrite(paddings, padding, 1, out)) return WRITE_ERROR;
    }
    return WRITE_OK;
}

