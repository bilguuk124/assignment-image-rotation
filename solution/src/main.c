#include "bmp-util/bmp_io.h"
#include "bmp-util/bmp-util.h"
#include "file/file.h"
#include "image/image.h"

#include <stdio.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if(argc != 3){
        printf("Не правильное количество аргументов! \n ");
        return 1;
    }
    FILE* input_image = open_file(argv[1],"r");
    FILE* output_image = open_file(argv[2],"w");
    struct image image = {0};
    if(from_bmp(input_image, &image) != READ_OK){
        printf("Ошибка в чтении файла! \n");
        close_file(input_image);
        close_file(output_image);
        return 1;
    }
    close_file(input_image);
    struct image rotated_image = rotate(&image);
    if(to_bmp(output_image,&rotated_image) != WRITE_OK){
        printf("Ошибка в записи файла!"); 
        close_file(output_image);
        return 1;
    }
    close_file(output_image);
    free(image.pixels);
    free(rotated_image.pixels);
    return 0;
}

