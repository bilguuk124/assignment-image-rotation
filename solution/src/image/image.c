
#include "image/image.h"

#include <malloc.h>

static void update_image(struct image* out, struct image const* source, int i, int j){
    out->pixels[i * out->width + j] = (source->pixels[(source->height - 1 - j) * source->width + i]);
}

struct image rotate(struct image const* source){
    struct image out = {0};
    out.width = source->height;
    out.height = source->width;
    out.pixels = malloc(sizeof(struct pixel) * source->width * source->height);
    for(size_t i =0; i <source->width; i++){
        for(size_t j = 0; j < source->height; j++){
            update_image(&out, source, i ,j);
        }
    }
    return out;
}
